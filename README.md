# IR Control

This Snips app requires Python 3

The IR Control app monitors and accesses IR Clients running on RPi. Check out the IR Client github https://bitbucket.org/oziee/irclient  

The default web adress to access the Snips app skill is http://<yours_snips_server>:7777

The port number can be configured in the `config.ini` file

Console link https://console.snips.ai/store/en/skill_EVp72n4aKe

### Interation Examples
_____

If there is only one IR Client you can simplify your statements  
`"turn the TV on"`  
`"switch the fan off"`  

If you have multiple IR Clients setup  
`"turn the lounge room tv on"`  
`"switch the bedroom fan off"`  

Even with multiple IR Client devices, if only one of these has the item for TV, you can still short hand it  
`"turn the tv off"`  

The software will search all devices known for any remote that matches  

You can call your built Action scripts  
`"play my Media Centre action"`  

There is even a sequence intent. Here you tell the software you want to just call out commands to follow, and when finish tell the software your sequence commands have ended  
`"please start for me sequence commands"`  
`"turn the tv on"`  
`"channel up"`  
`v"olume up"`  
`o"k stop sequence"`  

The software will keep the known `device` and `remote` used. If you call a function button without a remote, and the previous remote if called does not have that button function, it will ask you to say again but with more information.   


### Web Manager for IR Control
_____

connect to your web manager page http://<yours_snips_server>:<port>  

![devices](https://bitbucket.org/oziee/ircontrol/raw/2172ee4a9a08ccd93bd3a95c081e0c138f1b9948/static/images/devices.jpg)  
The devices menu lists all known and saved information about IR Client devices found on the network. It allows you to construct and edit easy voice abled synonyms to interact with Snips voice platform.  
(1) - Main menu  
(2) - Tabed manu of all devices found on the network  
(3) - Deletes the selected device  
(4) - List of all remotes the selected device has configured in lirc  
(5) - Remote synonyms. For Snips console you can give items an alternate word to match  
(6) - Function buttons for the selected remote from (4)  
(7) - The name of the function button lirc has for the selected remote  
(8) - Function button synonyms. For Snips console you can give items an alternate word to match  
(9) - Grid functions. Edit, complete edit or cancel edit, and delete item   

***If you delete an item (device, remote, function), but the item is still in the devices lirc list of information, the item will be re-added the next time the page is loaded)***

![noremotes](https://bitbucket.org/oziee/ircontrol/raw/2172ee4a9a08ccd93bd3a95c081e0c138f1b9948/static/images/noremotes.jpg)  

If a device is found on the network and lirc is installed and running, but does not have any remotes configured the device page will display this notice.

![actions](https://bitbucket.org/oziee/ircontrol/raw/2172ee4a9a08ccd93bd3a95c081e0c138f1b9948/static/images/actions.jpg)  
The actions menu allows you to build a set of IR Control Actions for IR Clients to process from one voiced instruction, instead of having to say each item to Snips one at a time.  
(1) - Action menu  
(2) - Dropdown list of Actions that have been build to load. You can then edit or delete the action  
(3) - Create a new action.  
(4) - Action Title. Must be unique.  
(5) - Action synonyms. For Snips console you can give items an alternate word to match this action  
(6) - Delete the selected action from (2)  
(7) - The IR Client device  
(8) - The remote from (7)  
(9) - The function button of (8)  
(10) - The selected (7)(8)(9) wait time after this function is made before the next function is started (pause time in seconds(12))  
(11) - If the function button needs to be held down for a period of time so that the device reacts (some Sony products)  
(12) - The period of time in seconds to hold the function button down if (11) is checked  
(13) - Add new item to the list  
(14) - Delete the action function row  

You can drag and drop rows to re-order their function in the queue

![intentbuilder](https://bitbucket.org/oziee/ircontrol/raw/2172ee4a9a08ccd93bd3a95c081e0c138f1b9948/static/images/intentbuilder.jpg)  

(1) - Intent Builder menu  
(2) - Slot items for the `ir_control` intent within the IR Control app in the Snips Console  
(3) - Slot items to be copied and pasted into the console within the `device` slot in `ir_control`  
(4) - Slot items to be copied and pasted into the console within the `remote` slot in `ir_control`  
(5) - Slot items to be copied and pasted into the console within the `button` slot in `ir_control`  

There is also the slots for the `ir_action` to be placed in the `action` slots

***All old items if no longer being used should be removed from the console slots before pasting in the current known slots names and synoynms from the Intent Builder page***