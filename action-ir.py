#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### **************************************************************************** ###
#
# Project: Calculator Skill for Snips
# Created Date: Wednesday, January 30th 2019, 6:41:12 pm
# Author: Greg
# -----
# Last Modified: Fri Apr 19 2019
# Modified By: Greg
# -----
# Copyright (c) 2019 Greg
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
# AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
### **************************************************************************** ###



import configparser
#from hermes_python.hermes import Hermes
#from hermes_python.ffi.utils import MqttOptions
#from hermes_python.ontology import *
from flask import Flask, url_for, render_template, abort, jsonify,request
#from snipskit.hermes.apps import HermesSnipsApp
from snipskit.config import SnipsConfig,MQTTConfig
#from snipskit.hermes.decorators import intent
#from snipskit.components import SnipsComponent
import io, sys, os
from flask_mqtt import Mqtt
import time
import eventlet
import json
import threading


CONFIGURATION_ENCODING_FORMAT = "utf-8"
CONFIG_INI = "config.ini"
devicesThatAnswered = []
currentSessionRunning = False
actionCurrentRunning = False


eventlet.monkey_patch()

class SnipsConfigParser(configparser.RawConfigParser):
    def to_dict(self):
        return {section : {option_name : option for option_name, option in self.items(section)} for section in self.sections()}

def read_snips_configuration_file(configuration_file):
    try:
        with io.open(configuration_file, encoding=CONFIGURATION_ENCODING_FORMAT) as f:
            conf_parser = SnipsConfigParser()
            conf_parser.optionxform = str 
            #conf_parser = configparser.ConfigParser()
            conf_parser.read_file(f)
            return conf_parser.to_dict()
            #return conf_parser
    except (IOError, configparser.Error) as e:
        return dict()

def write_configuration_file():
    global irDevices

    try:
        with open('data/data.ini', 'w') as configfile:
            json.dump(irDevices, configfile, indent=2)
    except Exception as e:
        print("Error saving configfile: {}".format(e))

def read_configuration_file():
    try:
        with open('data/data.ini', 'r') as configfile:
            return json.load(configfile)
    except Exception as e:
        print("Error Reading configfile: {}".format(e))
        return dict()

def create_default_device_db(device_name):
    global irDevices
    d = {'ERROR':True,"REMOTES":{}}
    irDevices['DEVICES'][device_name] = d
    write_configuration_file()

def addDevice(device_name):
    global devicesThatAnswered
    devicesThatAnswered.append(device_name)
    if irDevices == None:
        create_default_device_db(device_name)
    if not device_name in irDevices.keys():
        create_default_device_db(device_name)


irDevices = read_configuration_file()

if not "BUTTONS" in irDevices:
    btns = {}
    with open('data/buttonlist.txt', 'r') as buttonfile:
        line = buttonfile.readline()
        while line:
            btns[line.strip()] = ''
            line = buttonfile.readline()
        irDevices["BUTTONS"] = btns
        write_configuration_file()



app = Flask(__name__)
app.debug = False
app.config.from_pyfile('config.py')
app.url_map.strict_slashes = False

skconfig = SnipsConfig()
skconfig = skconfig.mqtt

conf = read_snips_configuration_file(CONFIG_INI)
app.config['MQTT_BROKER_URL'] = skconfig.broker_address.split(":")[0]
app.config['MQTT_BROKER_PORT'] = int(skconfig.broker_address.split(":")[1])
app.config['MQTT_KEEPALIVE'] = 120
app.config['MQTT_TLS_ENABLED'] = False

if skconfig.username:
    app.config['MQTT_USERNAME'] = skconfig.username
    app.config['MQTT_PASSWORD'] = skconfig.password
    

# Parameters for SSL enabled
"""
if 'mqtt_tls_hostname' in skconfig['snips-common']:
    app.config['MQTT_TLS_ENABLED'] = True
#if 'key1' in skconfig['snips-common']:
#    app.config['MQTT_TLS_INSECURE'] = skconfig['snips-common']['mqtt']
if 'mqtt_tls_cafile' in skconfig['snips-common']:
    app.config['MQTT_TLS_CA_CERTS'] = "{}{}".format(skconfig['snips-common']['mqtt_tls_capath'],
                                            skconfig['snips-common']['mqtt_tls_cafile'])
## MQTT TLS configuration
# mqtt_tls_hostname = ""
# mqtt_tls_disable_root_store = false
# mqtt_tls_cafile = ""
# mqtt_tls_capath = ""
# mqtt_tls_client_cert = ""
# mqtt_tls_client_key = ""
"""

def remove_dups(x): 
    return list(dict.fromkeys(x))


#============================================================== 
# MQTT
#==============================================================

mqtt = Mqtt(app)


def runAction(client,whichaction,actionCurrentRunning):
    for item in whichaction['actions']:
        hold = item['Hold']
        holdtime = item['HoldTime']
        pause = item['Pause']
        
        msg = {'remote':item['Remote'],
                'function':item['Function'],
                'hold':hold,
                'holdtime':holdtime,
                'pause':pause}
        client.publish('ir_control/{}'.format(item['Device']), json.dumps(msg))

        if hold:
            time.sleep(holdtime)
            time.sleep(0.2)
        if pause > 0:
            time.sleep(pause)
            time.sleep(0.2)
    actionCurrentRunning = False



@mqtt.on_connect()
def on_connect(client, userdata, flags, rc):
    if rc==0:
        #print("connected OK Returned code=",rc)
        client.subscribe('ir_control/info')
        client.subscribe('hermes/intent/ozie:ir_control')
        client.subscribe('hermes/intent/ozie:ir_action')
        client.subscribe('hermes/intent/ozie:ir_sequence')
        client.subscribe('hermes/intent/ozie:ir_cancel')
        client.subscribe('hermes/dialogueManager/sessionEnded')
        client.connected_flag=True #set flag
    else:
        print("Bad connection Returned code= ",rc)

@mqtt.on_disconnect()
def on_disconnect():
    mqtt.unsubscribe_all()

@mqtt.on_log()
def handle_logging(client, userdata, level, buf):
    #print(level, buf)
    pass

#@mqtt.on_message()
@mqtt.on_topic('ir_control/info')
def device_reply_message(client, userdata, message):
    global irDevices
    data = json.loads(message.payload.decode())

    device_name = data['device']
    addDevice(device_name)

    if data['error'] == False:
        irDevices['DEVICES'][device_name]['ERROR'] = False
        #no errors so see if there are any changes
        for item in data['data']:
            for key,val in item.items():
                #{'device': 'zero', 'data': [{'N2QAYB000352': ['BTN_LEFT', 'BTN_RIGHT']}], 'error': False}
                if not 'REMOTES' in irDevices['DEVICES'][device_name].keys():
                    irDevices['DEVICES'][device_name]['REMOTES'] = {}
                    
                if not key in irDevices['DEVICES'][device_name]['REMOTES']:
                    irDevices['DEVICES'][device_name]['REMOTES'][key] = "" #this will hold Synonyms
                    irDevices['DEVICES'][device_name][key] = []
                    #for btn in val:
                    irDevices['DEVICES'][device_name][key] = val# = "" #this will hold Synonyms
                else:
                    #remove already there.. is there any new buttons to add??
                    #for btn in val:
                    #    if not btn in irDevices['DEVICES'][device_name][key]:
                    irDevices['DEVICES'][device_name][key] = val #[btn] = "" #this will hold Synonyms
    else:
        irDevices['DEVICES'][device_name]['ERROR'] = True

    #save it
    write_configuration_file()

@mqtt.on_topic('hermes/intent/ozie:ir_control')
def ir_control_message(client, userdata, message):
    global irDevices,currentSessionRunning
    data = json.loads(message.payload.decode())
    #available_slots = data['customData'] or {}
    available_slots = {}
    if data['customData']:
        available_slots = json.loads(data['customData'])

    device_slot = ''
    remote_slot = ''
    button_slot = ''

    for slot in data['slots']:
        if slot['slotName'] == 'device':
            device_slot = slot['value']['value']
        if slot['slotName'] == 'remote':
            remote_slot = slot['value']['value']
        if slot['slotName'] == 'button':
            button_slot = slot['value']['value']

    if device_slot == '':
        if "device" in available_slots:
            device_slot = available_slots["device"]

    if remote_slot == '':
        if "remote" in available_slots:
            remote_slot = available_slots["remote"]

    if button_slot == '':
        if "button" in available_slots:
            button_slot = available_slots["button"]


    available_slots["device"] = device_slot
    available_slots["remote"] = remote_slot
    available_slots["button"] = button_slot

    #if not device_slot:
    #    msg = {'sessionId':data['sessionId'], 
    #            'text': 'for which IR device',
    #            'intentFilter':['ozie:ir_control']}
    #    client.publish('hermes/dialogueManager/continueSession',json.dumps(msg))
    if not remote_slot or not button_slot:
        msg = {'sessionId':data['sessionId'], 
                'text': 'for which remote or button to control',
                'customData': json.dumps(available_slots),
                'intentFilter':['ozie:ir_control']}
        #print("continuesession")
        return client.publish('hermes/dialogueManager/continueSession',json.dumps(msg))
    else:
        if device_slot:
            if not remote_slot in irDevices['DEVICES'][device_slot]['RMEOTES']:
                msg = {'sessionId':data['sessionId'], 
                        'text': 'this device does not have that remote. say a different remote or a new device',
                        'customData': json.dumps(available_slots),
                        'intentFilter':['ozie:ir_control']}
                #print("continuesession")
                return client.publish('hermes/dialogueManager/continueSession',json.dumps(msg))
        else:
            for d in irDevices['DEVICES']:
                if remote_slot in irDevices['DEVICES'][d]["REMOTES"]:
                    if button_slot in irDevices['DEVICES'][d][remote_slot]:
                        device_slot = d
            if device_slot == '':
                msg = {'sessionId':data['sessionId'], 
                        'text': 'could not find a remote and button matching what you said. try again',
                        'customData': json.dumps(available_slots),
                        'intentFilter':['ozie:ir_control']}
                #print("continuesession")
                return client.publish('hermes/dialogueManager/continueSession',json.dumps(msg))

        if device_slot and remote_slot and button_slot:
            if currentSessionRunning == False:
                #print('IR control.. non sequnce.. done')
                msg = {'sessionId':data['sessionId'], 'text':'ok'}
                client.publish('hermes/dialogueManager/endSession',json.dumps(msg))
            else:
                #print('IR control.. sequence.. done.. continueing')
                msg = {'sessionId':data['sessionId'], 
                        'text':'',
                        'customData': json.dumps(available_slots),
                        'intentFilter':['ozie:ir_control','ozie:ir_cancel']}
                #print("continuesession")
                client.publish('hermes/dialogueManager/continueSession',json.dumps(msg))
                
            msg = {'remote':remote_slot,
                    'function':button_slot,
                    'hold':False,
                    'holdtime':0,
                    'pause':0}
            client.publish('ir_control/{}'.format(device_slot), json.dumps(msg))

@mqtt.on_topic('hermes/intent/ozie:ir_action')
def ir_control_message(client, userdata, message):
    global irDevices, actionCurrentRunning
    
    data = json.loads(message.payload.decode())
    
    msg = {}

    if actionCurrentRunning:
        msg = {'sessionId':data['sessionId'], 
                'text': 'I cant start that action. A prior action has not finished yet.'}
        client.publish('hermes/dialogueManager/endSession',json.dumps(msg))


    if len(data['slots']) == 0:
        msg = {'sessionId':data['sessionId'], 
                'text': 'which IR action name',
                'intentFilter':['ozie:ir_action'],
                'slot':'action'}
        #print("action.. continue.. missing slot")
        return client.publish('hermes/dialogueManager/continueSession',json.dumps(msg))

    action_slot = data['slots'][0]
    action_name = action_slot['value']['value']
    whichaction = None
    if action_name in irDevices['ACTIONS']: 
        whichaction = irDevices['ACTIONS'][action_slot['value']['value']]
        actionCurrentRunning = True
        msg = {'sessionId':data['sessionId'], 
                'text': 'Action {} started'.format(action_name)}
    else:
        msg = {'sessionId':data['sessionId'], 
                'text': 'There does not seem to be an action by the name {} to run'.format(action_name)}

    #print("action control running")
    client.publish('hermes/dialogueManager/endSession', json.dumps(msg))
    if whichaction:
        threading.Thread(target=runAction, args=(client,whichaction,actionCurrentRunning)).start()

@mqtt.on_topic('hermes/intent/ozie:ir_sequence')
def ir_control_message(client, userdata, message):
    global currentSessionRunning
    data = json.loads(message.payload.decode())
    currentSessionRunning = True
    msg = {'sessionId':data['sessionId']}
    #client.publish('hermes/dialogueManager/endSession',json.dumps(msg))
    msg = {'sessionId':data['sessionId'], 
                'text': 'ok go',
                'intentFilter':['ozie:ir_control','ozie:ir_cancel']}
                
    client.publish('hermes/dialogueManager/continueSession',json.dumps(msg))
    #print("starting new sequence session because of errors")
    #_init = {'type':'action','canBeEnqueued':True,'intentFilter':['ozie:ir_control','ozie:ir_cancel']}
    #msg = {'siteId':data['siteId'], 
    #        'init': _init}
    #client.publish('hermes/dialogueManager/startSession',json.dumps(msg))
    #print("starting sequence control")

@mqtt.on_topic('hermes/intent/ozie:ir_cancel')
def ir_control_message(client, userdata, message):
    global currentSessionRunning
    data = json.loads(message.payload.decode())
    msg = {'sessionId':data['sessionId']}
    currentSessionRunning = False
    client.publish('hermes/dialogueManager/endSession',json.dumps(msg))
    #print("cancelling sequence control")

@mqtt.on_topic('hermes/dialogueManager/sessionEnded')
def session_ended_message(client, userdata, message):
    global currentSessionRunning
    data = json.loads(message.payload.decode())
    if data['termination']['reason'] == 'timeout':
        if currentSessionRunning == True:
            #start a new session
            #print("starting new sequence session because the other one ended")
            _init = {'type':'action','canBeEnqueued':True,'intentFilter':['ozie:ir_control','ozie:ir_cancel']}
            msg = {'siteId':data['siteId'], 
                    'init': _init}
            client.publish('hermes/dialogueManager/startSession',json.dumps(msg))
    else:
        currentSessionRunning = False



#============================================================== 
# IR control calls
#==============================================================

def findDeviceForRemote(remote, button):
    global irDevices
    pass

#============================================================== 
# App Routes and API calls
#==============================================================

@app.route('/')
def lets_begin():
    global devicesThatAnswered
    devicesThatAnswered = []
    if mqtt.client.connected_flag == True:
        mqtt.client.publish('ir_control/hello','') # we want all the IR devices to reply
    return render_template('index.html')

@app.route('/api/getdeviceinfo')
def api_devices():
    global devicesThatAnswered, irDevices
    return jsonify({'answered':devicesThatAnswered,
                    'devices':irDevices['DEVICES'],
                    'actions':irDevices['ACTIONS'], 
                    'buttons':irDevices['BUTTONS']})

@app.route('/api/updatedeviceremote', methods=['POST'])
def api_updatedeviceremote():
    global irDevices
    req_data = request.get_json()
    #{'device': 'zero', 'remote': 'N2QAYB000352', 'syns': 'TV, lounge tvs'}
    irDevices['DEVICES'][req_data['device']]['REMOTES'][req_data['remote']] = req_data['syns']
    write_configuration_file()
    return jsonify({})

@app.route('/api/deletedeviceremote', methods=['POST'])
def api_deletedeviceremote():
    global irDevices
    req_data = request.get_json()
    #{'device': 'zero', 'remote': 'N2QAYB000352'}
    del irDevices['DEVICES'][req_data['device']]['REMOTES'][req_data['remote']]
    del irDevices['DEVICES'][req_data['device']][req_data['remote']]
    write_configuration_file()
    return jsonify({})

@app.route('/api/updatebuttonsyn', methods=['POST'])
def api_updatebuttonsyn():
    global irDevices
    req_data = request.get_json()
    #{'device': 'zero', 'remote': 'N2QAYB000352', 'button': 'BTN_POWER', 'syns': 'TV, lounge tvs'}
    #irDevices['DEVICES'][req_data['device']][req_data['remote']][req_data['button']] = req_data['syns']
    #if not req_data['button'] in irDevices['BUTTONS']:
    #
    irDevices['BUTTONS'][req_data['button']] = req_data['syns']
    write_configuration_file()
    return jsonify({})

@app.route('/api/deletedeviceremotebutton', methods=['POST'])
def api_deletedeviceremotebutton():
    global irDevices
    req_data = request.get_json()
    #{'device': 'zero', 'remote': 'N2QAYB000352', 'button': 'BTN_POWER'}
    irDevices['DEVICES'][req_data['device']][req_data['remote']].remove(req_data['button'])
    write_configuration_file()
    return jsonify({})

@app.route('/api/deletedevice', methods=['POST'])
def api_deletedevice():
    global irDevices
    req_data = request.get_json()
    #{'device': 'zero'}
    del irDevices['DEVICES'][req_data['device']]
    write_configuration_file()
    return jsonify({})

@app.route('/api/actionsave', methods=['POST'])
def api_actionsave():
    global irDevices
    req_data = request.get_json()
    #{'device': 'zero', 'remote': 'N2QAYB000352', 'button': 'BTN_POWER', 'syns': 'TV, lounge tvs'}
    irDevices['ACTIONS'][req_data['key']] = req_data['data']
    write_configuration_file()
    return jsonify({})

@app.route('/api/deleteaction', methods=['POST'])
def api_deleteaction():
    global irDevices
    req_data = request.get_json()
    #{'actionkey': 'ASDJNA'} - key
    del irDevices['ACTIONS'][req_data['actionkey']]
    write_configuration_file()
    return jsonify({})




if __name__ == "__main__":
    app.config['PORT'] = int(conf['secret']['WEB_PORT'])
    app.run(host='0.0.0.0', port=app.config['PORT'])


