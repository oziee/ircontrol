/**
 * File: /Users/gregbail/LEDIR/static/index.js
 * Project: Snips Web Admin
 * Created Date: Saturday, April 6th 2019, 6:34:48 pm
 * Author: Greg
 * -----
 * Last Modified: Wed Apr 17 2019
 * Modified By: Greg
 * -----
 * Copyright (c) 2019 Your Company
 * ------------------------------------
 * Javascript will save your soul!
 */

var devices = {};
var actions = {};
var buttons = {};
var editingAction = {};

var currentActionTitle='';
var actionTitleHasError = false

var actionDeviceSelectEl = $('<select/>',{id:"gridDeviceSelect",class:"ddd"});
var actionDeviceESelectEl = $('<select/>',{id:"gridDeviceESelect",class:"ddd"});
var gridactionselecteddevice;
$(actionDeviceSelectEl).attr("onchange", "loadGridActionsRemotesDropdown(this)");
$(actionDeviceSelectEl).attr("data-none-selected-text", "Select Device");

var actionRemoteSelectEl = $('<select/>',{id:"gridRemoveSelect",class:"ddd"});
var actionRemoteESelectEl = $('<select/>',{id:"gridRemoveSelect",class:"ddd"});
var gridactionselectedremote;
$(actionRemoteSelectEl).attr("onchange", "loadGridActionsButtonDropdown(this)");
$(actionRemoteSelectEl).attr("data-none-selected-text", "Select A Device First");

var actionButtonSelectEl = $('<select/>',{id:"gridButtonSelect",class:"ddd"});
$(actionButtonSelectEl).attr("data-none-selected-text", "Select A Remote");

var selectedDevice = '';
var selectedRemote = '';
var devicesThatAnswered = [];
var firstdeviceloaded = false;
var griddata1, griddata2, gridRemote, gridButton, dialog;

$('#actionTitle').limitkeypress({ rexp: /^[A-Za-z0-9_]*$/ });
$('#actionTitleSynonyms').limitkeypress({ rexp: /^[A-Za-z0-9, ]*$/ });


function hidealldivs() {
    $('#deviceOptions').hide();
    $('#deviceHasErrors').hide();
    $('#deviceNoRemotes').hide();
    $('#deviceinfo').hide();
}
hidealldivs();

var timer;
function removepreloader() {
    clearTimeout(timer);
    //get data
    loadData();
    $(".preloader").fadeOut();
}

$(document).ready(function() {
    //socket.emit('getDevices',{});
    timer = setTimeout(removepreloader, 3000);
});

//load info
function loadData() {
    $('#myTab').hide();
    $('#actionsdiv').hide();
    $.getJSON("/api/getdeviceinfo", function(data) {
        devicesThatAnswered = data['answered'];
        devices = data['devices'];
        actions = data['actions'];
        buttons = data['buttons'];
        $.each(data['devices'],function(key,value){ 
            addDeviceTab(key);
        });
        if (Object.keys(actions).length > 0) {
            //there is something to show
            loadFirstAction();
        }
        else {
            $('#actionbuildercard').hide();
        }
    });
}

function addDeviceTab(devicename) {
    if (firstdeviceloaded == false) {
        //so that the first tab is set to active
        $('.nodevices').hide();
        $('#actionsdiv').show();
        $('#myTab').show();
        selectedDevice = devicename;
        $('#myTab').append('<li class="nav-item" style="z-index: 991; font-weight:500;"><a class="nav-link active" href="#" data-toggle="tab">' + devicename + '</a></li>');
        loadDeviceInfoToPage(devicename);
        firstdeviceloaded = true;
    }
    else {
        $('#myTab').append('<li class="nav-item" style="z-index: 991; font-weight:500;"><a class="nav-link" href="#" data-toggle="tab">' + devicename + '</a></li>');
    }
}

function loadFirstAction() {
    $('#actionbuildercard').hide();
    var selectitems;

    $('#selectAction').empty();

    $.each(actions,function(i,value){ 
        if (value.synonyms) {
            selectitems += '<option value="' + i + '">' + value.title + ', ' + value.synonyms + '</option>'
        }
        else {
            selectitems += '<option value="' + i + '">' + value.title + '</option>'
        }
    });
    if (selectitems) {
        $("#selectAction").html(selectitems);
        $('#actionbuildercard').show();
        showactioncards($('#selectAction').val());
    }
    $('#selectAction').selectpicker('refresh');
}

$(document).on('click', '#btnCreateNewAction', function (e) {
    var rnd = Math.random().toString(36).substring(2, 7) + Math.random().toString(36).substring(2, 7);
    var tempName = "name_me_"+ rnd;
    editingAction={};
    actions[rnd] = {'title':tempName,"synonyms":"","actions":[]};
    saveAction(rnd);
    $("#selectAction").append('<option value="' + rnd + '">' + tempName + '</option>');
    $('#selectAction').selectpicker('refresh');
    $('#selectAction').selectpicker('val', rnd);
    showactioncards(rnd);
});

function saveAction(key) {
    var d = {"key":key,"data":actions[key]};
    $.ajax({
        url: '/api/actionsave',
        type: 'POST',    
        data: JSON.stringify(d),
        contentType: 'application/json',
        success: function(result) {
            
        },
        error: function(result){
            alert("There was an error saving new action");
        }
    });
}

function updateAction() {
    var key = $('#selectAction').val();
    actions[key]['title'] = $('#actionTitle').val();
    actions[key]['synonyms'] = $('#actionTitleSynonyms').val();
    actions[key]['actions'] = editingAction['actions']
    var d = {"key":key,"data":actions[key]};
    $.ajax({
        url: '/api/actionsave',
        type: 'POST',    
        data: JSON.stringify(d),
        contentType: 'application/json',
        success: function(result) {
            
        },
        error: function(result){
            alert("There was an error updating the action");
        }
    });
}

$(document).on('blur', '#actionTitle', function (e) { 
    if (actionTitleHasError == false) {
        changeDropdownTexts();
    }
});

//making sure the Action Title is unique
$(document).on('keyup', '#actionTitle', function (e) { 
    $.each(actions,function(key,value){
        if ($('#actionTitle').val() == value.title) {
            if ($('#actionTitle').val() == currentActionTitle) {
                $('#actionTitle').removeClass('actionerror');
                actionTitleHasError = false;
                $("#titleerror").hide();
            } else {
                $('#actionTitle').addClass('actionerror');
                actionTitleHasError = true;
                $("#titleerror").show();
            }
        }
        else {
            $('#actionTitle').removeClass('actionerror');
            actionTitleHasError = false;
            $("#titleerror").hide();
        }
    });
});

$(document).on('blur', '#actionTitleSynonyms', function (e) { 
    changeDropdownTexts();
});

function changeDropdownTexts() {
    updateAction();
    var selectitems;
    var selaction = $('#selectAction').val();

    $('#selectAction').empty();

    $.each(actions,function(i,value){ 
        if (value.synonyms) {
            selectitems += '<option value="' + i + '">' + value.title + ', ' + value.synonyms + '</option>'
        }
        else {
            selectitems += '<option value="' + i + '">' + value.title + '</option>'
        }
    });
    $("#selectAction").html(selectitems);
    $('#selectAction').selectpicker('refresh');
    $('#selectAction').selectpicker('val', selaction);
}

$(document).on('click', '#btnDeleteAction', function (e) {
    var key = $('#selectAction').val();
    var title = actions[key].title;
    var result = confirm("Delete " + title + ". Are you sure?");
    if (result) {
        var d = {"actionkey":key};
        delete actions[key];
        $.ajax({
            url: '/api/deleteaction',
            type: 'POST',    
            data: JSON.stringify(d),
            contentType: 'application/json',
            success: function(result) {
                //console.log("saved ok")
                loadFirstAction();
            },
            error: function(result){
                //console.log(result);
                alert("There was an error deleting " + key);
            }
        });
    }
    
});

$(document).on('change', '#selectAction', function (e) {
    showactioncards(this.value);
});

function showactioncards(key) {
    loaddropdowns();
    $('#actionTitle').removeClass('actionerror');
    actionTitleHasError = false;
    $("#titleerror").hide();
    editingAction=actions[key];
    currentActionTitle = editingAction['title'];
    $('#actionTitle').val(editingAction['title']);
    $('#actionTitleSynonyms').val(editingAction['synonyms']);
    $('#actionbuildercard').show();
    $("#actiongrid").jsGrid("loadData");
}

$(document).on('click', '#deletedevice', function (e) {
    var result = confirm("Delete " + selectedDevice + ". Are you sure?");
    if (result) {
        var d = {"device":selectedDevice};
        delete devices[selectedDevice];
        $.ajax({
            url: '/api/deletedevice',
            type: 'POST',    
            data: JSON.stringify(d),
            contentType: 'application/json',
            success: function(result) {
                //console.log("saved ok")
                var t = $("#myTab a.active").parent();//$("ul#myTab li.active")
                t.remove();
                var count = $("#myTab li").length;
        
                if (count > 0) {
                    var f = $("#myTab a").first();//.tab('show');
                    f.trigger('click');
                }
                else {
                    hidealldivs();
                    $('#myTab').hide();
                    $('#actionsdiv').hide();
                    $('.nodevices').show();
                }
                //$('[href="#menu3"]').tab('show');
            },
            error: function(result){
                //console.log(result);
                alert("There was an error deleting " + selectedDevice);
            }
        });
    }
});

$(document).on('click', '#myTab a', function (e) {
    e.preventDefault()
    hidealldivs();
    $(this).tab('show');
    var devicename =$(e.target).html();
    selectedDevice = devicename;
    loadDeviceInfoToPage(devicename);
});

var scrolltit = function(){
    var $id = $("#copy");
    var pos = $id.offset().top - 60;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos});
};

//link from the intent builder to the assistance tab to display help
$(document).on('click', '#assistantlink', function (e) {
    $('#nav-help-tab').trigger('click');
    $('#nav-intent-tab').removeClass('active');
    setTimeout(scrolltit, 500);
    
});

$(document).on('click', '#menu a', function (e) {
    e.preventDefault()
    var tb = $(e.target).attr('aria-controls');

    if (tb == 'nav-actions') {
        loaddropdowns();
    }
    if (tb == 'nav-help') {
        //console.log('help')
    }
    if (tb == 'nav-intent') {
        intentbuilder();
    }
});

function loaddropdowns() {
    gridactionselecteddevice = '';
    $(actionDeviceSelectEl).empty();
    $(actionDeviceESelectEl).empty();
    $(actionRemoteSelectEl).empty();
    $(actionRemoteESelectEl).empty();
    $(actionButtonSelectEl).empty();

    $(actionDeviceSelectEl).parent().removeClass('jsgrid-invalid')
    $(actionRemoteSelectEl).parent().removeClass('jsgrid-invalid')
    $(actionButtonSelectEl).parent().removeClass('jsgrid-invalid')
    
    //var my_list = $(actionDeviceSelectEl).empty();
    $(actionDeviceSelectEl).append($("<option>").attr('value','').text('')); //add blank item

    $.each(devices,function(key,value){
        $(actionDeviceSelectEl).append($("<option>").attr('value',key).text(key));
        $(actionDeviceESelectEl).append($("<option>").attr('value',key).text(key));
    });
}

function loadGridActionsRemotesDropdown(ddb) {
    $(actionButtonSelectEl).empty();
    
    $(actionDeviceSelectEl).parent().removeClass('jsgrid-invalid')
    $(actionRemoteSelectEl).parent().removeClass('jsgrid-invalid')
    $(actionButtonSelectEl).parent().removeClass('jsgrid-invalid')
    
    //$(actionButtonSelectEl).selectpicker('refresh');

    if (ddb.value == ''){
        $(actionRemoteSelectEl).empty();
        $(actionRemoteESelectEl).empty();
    } else {
        $(actionRemoteSelectEl).empty();
        $(actionRemoteESelectEl).empty();
        $(actionRemoteSelectEl).append($("<option>").attr('value','').text('')); //add blank item
        //$(actionRemoteESelectEl).append($("<option>").attr('value','').text('')); //add blank item
        $.each(devices[ddb.value]['REMOTES'],function(key,value){
            $(actionRemoteSelectEl).append($("<option>").attr('value',key).text(key));
            $(actionRemoteESelectEl).append($("<option>").attr('value',key).text(key));
        });
    }
    gridactionselecteddevice = ddb.value;
    //$(actionRemoteSelectEl).selectpicker('refresh');
    //$('.dropdown-toggle').css("background-color", "#fff");
}

function loadGridActionsButtonDropdown(ddb) {
    if (ddb.value == ''){
        $(actionButtonSelectEl).empty();
    } else {
        var my_list = $(actionButtonSelectEl).empty();
        devices[gridactionselecteddevice][ddb.value].forEach(function(element) {
            my_list.append($("<option>").attr('value',element).text(element));
        });
    }
    //$(actionButtonSelectEl).selectpicker('refresh');
    $('.dropdown-toggle').css("background-color", "#fff");
}

function loadDeviceInfoToPage(devicename) {

    deviceinfo = devices[devicename];
    $('#deviceOptions').show();

    //if the device did not answer display this message
    if (devicesThatAnswered.includes(devicename) ) {
        $('.devicedidnotanswer').hide();
    }
    else {
        $('.devicedidnotanswer').show();
    }

    if (deviceinfo['ERROR'] == true) {
        $('#deviceHasErrors').show();
    }
    else {
        //no error but does it have any remotes listed
        if (Object.keys(deviceinfo['REMOTES']).length == 0) {
            $('#deviceNoRemotes').show();
        }
        else {
            //all good
            griddata1 = []
            griddata2 = []
            //deviceinfo['REMOTES'].forEach(function(entry) {
            $.each(deviceinfo['REMOTES'],function(remotekey,remotesyns){ 
                griddata1.push({ 'Remote': remotekey, 'Synonyms': remotesyns })
                //$.each(deviceinfo[remotekey],function(btns,btnsvalue){ 
                //    griddata2.push({ 'Function': btns, 'Synonyms': btnsvalue })
                //});
                
                deviceinfo[remotekey].forEach(function(element) {
                    griddata2.push({ 'Function': element, 'Synonyms': '' })
                });
                loadgrid();
            });
            
            $('#deviceinfo').show();
        }
    }
}

function loadRemoteButtonforGrid() {
    griddata2 = []; //reset

    devices[selectedDevice][selectedRemote].forEach(function(element) {
        griddata2.push({ 'Function': element, 'Synonyms': buttonSynomyn(element) });
    });
}

function buttonSynomyn(btn_name) {
    try {
        return buttons[btn_name];
    }
    catch(err) {
        return "";
    }
    
}

function loadgrid() {
    $("#gridRemotes").jsGrid("loadData");
}


// *** Remote Grid for device
$(document).ready(function () {

    //enter key pressed within the calling grid field will end edit and update to grid row
    function update_on_enter(field,grid) {
        field.on("keydown", function(e) {
          if(e.keyCode === 13) {
            $("#" + grid).jsGrid("updateItem");
            return false;
          }
        });
    }

    var DeviceField = function(config) {
        jsGrid.Field.call(this, config);
    };
    var RemoteField = function(config) {
        jsGrid.Field.call(this, config);
    };
    var ButtonField = function(config) {
        jsGrid.Field.call(this, config);
    };

    DeviceField.prototype = new jsGrid.Field({

        itemTemplate: function(value) {
            return value;//new Date(value).toDateString();
        },

        insertTemplate: function(value) {
            return this._insertPicker = actionDeviceSelectEl;//$("<input>").datepicker({ defaultDate: new Date() });
        },
        /*
        editTemplate: function(value) {
            return this._editPicker = actionDeviceESelectEl.val(value);//$("<input>").datepicker().datepicker("setDate", new Date(value));
        },
        */
        insertValue: function() {
            return this._insertPicker.val();
        }/*,

        editValue: function() {
            return this._editPicker.val();
        }*/
    });

    RemoteField.prototype = new jsGrid.Field({

        itemTemplate: function(value) {
            return value;//new Date(value).toDateString();
        },

        insertTemplate: function(value) {
            return this._insertPicker = actionRemoteSelectEl;//$("<input>").datepicker({ defaultDate: new Date() });
        },
        /*
        editTemplate: function(value) {
            return this._editPicker = actionRemoteESelectEl.val(value);//$("<input>").datepicker().datepicker("setDate", new Date(value));
        },
        */
        insertValue: function() {
            return this._insertPicker.val();
        }
    });

    ButtonField.prototype = new jsGrid.Field({

        itemTemplate: function(value) {
            return value;//new Date(value).toDateString();
        },

        insertTemplate: function(value) {
            return this._insertPicker = actionButtonSelectEl;//$("<input>").datepicker({ defaultDate: new Date() });
        },
        /*
        editTemplate: function(value) {
            return this._editPicker = actionButtonESelectEl.val(value);//$("<input>").datepicker().datepicker("setDate", new Date(value));
        },
        */
        insertValue: function() {
            return this._insertPicker.val();
        }
    });

    jsGrid.fields.DeviceField = DeviceField;
    jsGrid.fields.RemoteField = RemoteField;
    jsGrid.fields.ButtonField = ButtonField;

    var jsgridRemotes;
    $("#gridRemotes").jsGrid({
        height: "auto",
        width: "100%",
        sorting: false,
        paging: false,
        autoload: false,
        editing: true,
        noDataContent: "No remotes to show",
        deleteConfirm: function(item) {
            return item.Remote + " will be removed. Are you sure?";
        },
        onInit: function(args) {
            jsgridRemotes = args.grid;
        },
        controller: {
            loadData: function() {
                return griddata1;
            },
            updateItem: function(item) {
                var d = {"device":selectedDevice,"remote":item.Remote,"syns":item.Synonyms};
                devices[selectedDevice]['REMOTES'][item.Remote] = item.Synonyms;
                $.ajax({
                    url: '/api/updatedeviceremote',
                    type: 'POST',    
                    data: JSON.stringify(d),
                    contentType: 'application/json',
                    success: function(result) {
                        //console.log("saved ok")
                    },
                    error: function(result){
                        //console.log(result);
                        alert("There was an error updating the intent");
                    }
                });
            },
            deleteItem: function(item) {
                var d = {"device":selectedDevice,"remote":item.Remote};
                
                delete devices[selectedDevice]['REMOTES'][item.Remote];
                delete devices[selectedDevice][item.Remote];
                $.ajax({
                    url: '/api/deletedeviceremote',
                    type: 'POST',    
                    data: JSON.stringify(d),
                    contentType: 'application/json',
                    success: function(result) {
                        //console.log("saved ok")
                        
                        if (jsgridRemotes.data.length) {
                            //First find the {jsgrid-grid-body} to scroll the top
                            var gridBody = $("#gridRemotes").find('.jsgrid-grid-body');
                            //fire the click event of first row to select first item.
                            gridBody.find('.jsgrid-table tr:first-child').trigger('click');
                            //scroll to top after click event fire
                            gridBody.animate({
                                scrollTop: 0,
                                scrollLeft: 0
                            }, 250);
                        }
                        else {
                            griddata2 = [];
                            $("#gridButtons").jsGrid("loadData");
                        }
                    },
                    error: function(result){
                        //console.log(result);
                        alert("There was an error updating the intent");
                    }
                });
            }
        },
        onRefreshed: function(args) {
            //select the first row if there is one when grid loads
            //If in grid data has beedn load then length will > 0
            if (args.grid.data.length) {
                //First find the {jsgrid-grid-body} to scroll the top
                var gridBody = $("#gridRemotes").find('.jsgrid-grid-body');
                //fire the click event of first row to select first item.
                gridBody.find('.jsgrid-table tr:first-child').trigger('click');
                //scroll to top after click event fire
                gridBody.animate({
                    scrollTop: 0,
                    scrollLeft: 0
                }, 250);

            }
        },
        rowClick: function ( args ) {
            var $row = $(args.event.target).closest("tr");

            //if the other grid is editing update it
            if (jsgridButtons) {
                if(jsgridButtons._editingRow) {
                    jsgridButtons.updateItem();
                }
            }

            selectedRemote = args.item.Remote;
            $('#selectedremotecardtitle').html(selectedRemote + " Functions");
            
            loadRemoteButtonforGrid();

            $("#gridButtons").jsGrid("loadData");

            if(this._editingRow) {
                this.updateItem().done($.proxy(function() {
                    this.editing && this.editItem($row);
                }, this));
                return;
            }

            this.editing && this.editItem($row);
        },
        fields: [
            { name: "Remote", type: "text" , width: 250, editing:false},
            { name: "Synonyms", type: "text", width: 'auto', editing:true,
                editTemplate: function(value, item) { 
                    //this.editControl = $("<input>").val(value);
                    var $result = jsGrid.fields.text.prototype.editTemplate.call(this, value);
                    $result.limitkeypress({ rexp: /^[A-Za-z0-9 ,]*$/ });
                    update_on_enter(this.editControl, 'gridRemotes');
                    setTimeout(function() {
                        $result.focus();
                    });

                    this.editControl = $result
                    
                    return this.editControl;
                }
            },
            {
                type: "control",
                modeSwitchButton: true,
                editButton: true
            }
        ]
    });

    var jsgridButtons;
    $("#gridButtons").jsGrid({
        height: "auto",
        width: "100%",
 
        sorting: false,
        paging: false,
        autoload: false,
        editing: true,
        noDataContent: "No buttons for this remote to show",
        deleteConfirm: function(item) {
            return item.Function + " will be removed. Are you sure?";
        },
        onInit: function(args) {
            jsgridButtons = args.grid;
        },
        controller: {
            loadData: function() {
                return griddata2;
            },
            updateItem: function(item) {
                //var d = {"device":selectedDevice,"remote":selectedRemote, "button":item.Function,"syns":item.Synonyms};
                //devices[selectedDevice][selectedRemote][item.Function] = item.Synonyms;
                var d = {"button":item.Function,"syns":item.Synonyms};
                buttons[item.Function] = item.Synonyms;
                
                $.ajax({
                    url: '/api/updatebuttonsyn', //url: '/api/updatedeviceremotebutton',
                    type: 'POST',    
                    data: JSON.stringify(d),
                    contentType: 'application/json',
                    success: function(result) {
                        //console.log("saved ok")
                    },
                    error: function(result){
                        //console.log(result);
                        alert("There was an error updating the button info");
                    }
                });
            },
            deleteItem: function(item) {
                var d = {"device":selectedDevice,"remote":selectedRemote,"button":item.Function};
                
                //delete devices[selectedDevice][selectedRemote][item.Function];
                devices[selectedDevice][selectedRemote] = devices[selectedDevice][selectedRemote].filter(function(litem) { 
                    return litem !== item.Function
                })

                $.ajax({
                    url: '/api/deletedeviceremotebutton',
                    type: 'POST',    
                    data: JSON.stringify(d),
                    contentType: 'application/json',
                    success: function(result) {
                        //console.log("saved ok")
                    },
                    error: function(result){
                        //console.log(result);
                        alert("There was an error updating the intent");
                    }
                });
            }
        },
        rowClick: function ( args ) {
            var $row = $(args.event.target).closest("tr");

            if(this._editingRow) {
                this.updateItem().done($.proxy(function() {
                    this.editing && this.editItem($row);
                }, this));
                return;
            }

            this.editing && this.editItem($row);
        },
        fields: [
            { name: "Function", type: "text" , width: 250, editing:false},
            { name: "Synonyms", type: "text", width: 'auto', editing:true,
            editTemplate: function(value, item) { 
                var $result = jsGrid.fields.text.prototype.editTemplate.call(this, value);
                $result.limitkeypress({ rexp: /^[A-Za-z0-9 ,]*$/ });
                update_on_enter(this.editControl, 'gridButtons');
                setTimeout(function() {
                    $result.focus();
                });

                this.editControl = $result
                
                return this.editControl;
            }
            },
            {
                type: "control",
                modeSwitchButton: true,
                editButton: true
            }
        ]
    });

    var jsgridActions;
    $("#actiongrid").jsGrid({
        height: "auto",
        width: "100%",
        rowClass: function(item, itemIndex) {
            return "action-" + itemIndex;
        },
        inserting: true,
        paging: true,
        autoload: false,
        editing: true,
        deleteButton: true,   
        deleteButtonTooltip: "Delete",
        noDataContent: "Use the Add Row to start building an Action set",
        onInit: function(args) {
            jsgridActions = args.grid;
        },
        onDataLoaded: function(args) {
            args.grid.sort( "Step");
        },
        deleteConfirm: function(item) {
            return "This item will be removed. Are you sure?";
        },
        controller: {
            loadData: function() {
                return editingAction['actions'];
            }
        },
        onItemDeleted: function(args) {
            //console.log('onItemDeleted',args);
            // arrays of items
            var $gridData = $("#actiongrid .jsgrid-grid-body tbody");
            var items = $.map($gridData.find("tr"), function(row) {
                return $(row).data("JSGridItem");
            });

            //c = 1;                     
            $.each(items,function(index,value){
                value.Step = index+1;             
            });
            updateAction();
        },
        onItemInserting: function(args) {
            //console.log('onItemInserting',args);
            args.item.Step = args.grid.data.length + 1;
        },
        onItemInserted: function(args) {
            //console.log('onItemInserted',args);
            updateAction();
        },
        onItemUpdated: function(args) {
            //console.log('onItemUpdated',args);
            updateAction();
        },
        invalidNotify: function(args) {
            $('#alert-error-not-submit').removeClass('hidden');
        },
        fields: [ 
            { name: "Step", type: "number" , width: 80, editing:false,visible: false},
            { name: "Device", type: "DeviceField", width: 150, editing:false,validate: "required"},
            { name: "Remote", type: "RemoteField" , width: 150, editing:false,validate: "required"},
            { name: "Function", type: "ButtonField" , width: 150, editing:false,validate: "required"},
            { name: "Pause", title:"Pause (s)", type: "number", width: 80, editing:true,
                validate: { message: "numeric only", validator: function(value) { return value >= 0; } },
                editTemplate: function(value) {
                    var $result = jsGrid.fields.number.prototype.insertTemplate.call(this); // original input
                    $result.attr('min',0);
                    $result.limitkeypress({ rexp: /^[0-9]*$/ });
                    $result.val(value);
                    this.editControl = $result
                    update_on_enter(this.editControl, 'actiongrid');
                    return $result;
                },
                insertTemplate: function() {
                    var $result = jsGrid.fields.number.prototype.insertTemplate.call(this); // original input
                    $result.attr('min',0);
                    //$result.attr('pattern',"^[0-9]*$" );
                    $result.limitkeypress({ rexp: /^[0-9]*$/ });
                    $result.val(0);
                    return $result;
                } 
            },
            { name: "Hold", type: "checkbox", width: 100, editing:true , sortable: false, formatter: 'checkbox',editoptions:{value:"true:false"},edittype: "checkbox",align: "center",sorting: false},
            { name: "HoldTime", title:"For (s)", type: "number", width: 80, editing:true,
                validate: { message: "numeric only", validator: function(value) { return value >= 0; } },
                editTemplate: function(value) {
                    var $result = jsGrid.fields.number.prototype.insertTemplate.call(this); // original input
                    $result.attr('min',0);
                    $result.limitkeypress({ rexp: /^[0-9]*$/ });
                    $result.val(value);
                    this.editControl = $result
                    update_on_enter(this.editControl, 'actiongrid');
                    return $result;
                },
                insertTemplate: function() {
                    var $result = jsGrid.fields.number.prototype.insertTemplate.call(this); // original input
                    $result.attr('min',0);
                    $result.limitkeypress({ rexp: /^[0-9]*$/ });
                    $result.val(0);
                    return $result;
                } 
            },
            { type: "control",  editButton: false, modeSwitchButton: false }
        ],
        onRefreshing: function(args) {
            //$('.selectpicker').selectpicker('render');
        },
        onRefreshed: function(args) {
         
            //move the default new line from the top of the grid to the bottom
            //check to see if it has already been done, and if not do it
            var $gridData = $("#actiongrid .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    // array of indexes
                    var clientIndexRegExp = /\s*action-(\d+)\s*/;
                    var indexes = $.map($gridData.sortable("toArray", { attribute: "class" }), function(classes) {
                        return clientIndexRegExp.exec(classes)[1];
                    });
                    
                    // arrays of items
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });

                    //c = 1;                     
                    $.each(items,function(index,value){
                        value.Step = index+1;             
                    });
                    updateAction();

                    console && console.log("Reordered items", items);
                }
            });
        }
    });

});



//window resize events
$(function() {
    "use strict";

    var set = function() {
        //var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
        var topOffset = 57;

        var height = ((window.innerHeight > 0) ? window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $(".page-wrapper").css("min-height", (height) + "px");
            
            //$(".page-wrapper").css("min-height", "100vh");
        }
        //$("#list-actions").css("max-height", (this.screen.height-200) + "px");
        $("#list-actions").height(height-190)
    };
    $(window).ready(set);
    $(window).on("resize", set);
});

//Intent Builder Tab data creation
//this is called when the menu tab is clicked
function intentbuilder() {
    var buildstring = '';
    var remotesstring = '';
    var buttonsstring = '';

    $.each(devices,function(key,value){ 
        buildstring += key + "</br>";
        $.each(devices[key]['REMOTES'],function(remotekey,remotevalue){ 
            if (remotevalue == '') {
                remotesstring += remotekey + "</br>";
            } else {
                remotesstring += remotekey + ", " + remotevalue + "</br>";
            }
            devices[key][remotekey].forEach(function(element) {
                var syns = buttonSynomyn(element);
                if (syns == '') {
                    buttonsstring += element + "</br>";
                } else {
                    buttonsstring += element + ", " + syns + "</br>";
                }
            });
        });
    });
    setIntentBulderItem(buildstring,'builderDevices');
    setIntentBulderItem(remotesstring,'builderRemotes');
    setIntentBulderItem(buttonsstring,'builderButtons');

    //actions
    buildstring = '';
    $.each(actions,function(key,value){ 
        if (actions[key].synonyms == '') {
            buildstring += actions[key].title + "</br>";
        } else {
            buildstring += actions[key].title + ", " + actions[key].synonyms + "</br>";
        }
        
    });
    setIntentBulderItem(buildstring,'builderActions')

}

function setIntentBulderItem(buildstring,divitem) {
    var header = "****************************************</br>";
    if (buildstring == '') {
        $('#' + divitem).html('Nothing to do');
    } else {
        $('#' + divitem).html(header + buildstring + header);
    }
}